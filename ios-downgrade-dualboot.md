## Downgrade and dualboot status of almost all iOS devices

UPDATED: 2024-12-01

- Reddit mirror: https://www.reddit.com/r/iOSDowngrade/comments/j1x7iv/downgrade_and_dualboot_status_of_almost_all_ios/
- GitHub Gist mirror: https://gist.github.com/LukeZGD/9d781f1b03a69fa46869384a9407a41a
-	As of updating this post, there is a downgrade tool that utilizes a SEP exploit released. However its device support is currently limited: https://limefix.tech/?product=limefix-sep-utility-testing
    - There might be a better downgrade tool that uses a SEP exploit in the future, it may be better to wait for that instead
-	Save SHSH blobs for signed iOS versions using [TSS Saver](https://tsssaver.1conan.com/) or [blobsaver](https://github.com/airsquared/blobsaver)
- GitHub repo for futurerestore nightly: https://github.com/futurerestore/futurerestore
- For Windows builds of futurerestore nightly, go to the Discord server linked in the readme
- Almost all 32-bit downgrades can be done by Legacy iOS Kit: https://github.com/LukeZGD/Legacy-iOS-Kit
- Tethered 64-bit downgrades are mostly covered by Semaphorin (discontinued): https://github.com/LukeZGD/Semaphorin
- Tethered 64-bit iOS 15 downgrade guide by dleovl: https://github.com/dleovl/ios15tether
- Tethered 64-bit downgrade guide by mineek: https://github.com/mineek/iostethereddowngrade
- Guide for downgrading iOS 16+ checkm8 devices to iOS 14-15: https://gist.github.com/pixdoet/2b58cce317a3bc7158dfe10c53e3dd32
- Delay OTA Guide (updating only): https://ios.cfw.guide/updating-blobless
- Even if there are Windows builds/versions for some of these tools, **iOS downgrades should be done on macOS or Linux only**

### **iOS 17/18 devices** (A12 devices and newer, older A10 iPads)
- Devices that fall under here have the latest version 17.x/18.x.
- Delay OTA Guide (updating only): https://ios.cfw.guide/updating-blobless
- (A12 devices and newer) You cannot restore to any iOS versions other than signed ones. All SHSH blobs are currently useless.
- For A10 iPads only:
    - Tethered downgrade/dualboot: [Semaphorin](https://github.com/LukeZGD/Semaphorin) supports these devices for downgrading to lower iOS versions
    - See links above for other tethered 64-bit downgrade guides
    - It is technically possible to downgrade to iOS 14-15 with blobs. For advanced users only, see "Guide for downgrading iOS 16+ checkm8 devices to iOS 14-15" linked above

### **iOS 16 devices** (A11 devices, older A9 iPads)
- Devices that fall under here have the latest iOS version 16.x.
- See the [SEP/BB Compatibility Chart](https://docs.google.com/spreadsheets/d/1Mb1UNm6g3yvdQD67M413GYSaJ4uoNhLgpkc7YKi3LBs/). iOS 16 SEP/baseband is incompatible with iOS 15 and below.
- These devices can be restored to iOS 16.6.x with SHSH blobs, you may use [Legacy iOS Kit](https://github.com/LukeZGD/Legacy-iOS-Kit) for this which utilizes the updated **futurerestore nightly**.
- For iPhone X, you cannot restore to any iOS versions other than signed ones or 16.6.x with blobs. All SHSH blobs for 16.5.x and lower are currently useless.
- Tethered downgrade/dualboot: [Semaphorin](https://github.com/LukeZGD/Semaphorin) supports these devices for downgrading to lower iOS versions
    - Downgrading from iOS 16 is supported on macOS only
- Tethered downgrade/dualboot is also an option with [downr1n](https://github.com/edwin170/downr1n) or [dualra1n](https://github.com/dualra1n/dualra1n)
- See links above for tethered 64-bit downgrade guides (likely not applicable to iPhone X)
- It is technically possible to downgrade to iOS 14-15 with blobs for all these devices except for iPhone X. For advanced users only. See "Guide for downgrading iOS 16+ checkm8 devices to iOS 14-15" linked above

###	**iOS 14-15 devices** (A9 and A10 devices, iPad Air 2 and mini 4)
- Devices that fall under here have the latest iOS version 15.x.
- These devices can be restored to iOS 14.x/15.x with SHSH blobs, you may use [Legacy iOS Kit](https://github.com/LukeZGD/Legacy-iOS-Kit) for this which utilizes the updated **futurerestore nightly**.
- For more details about SEP and baseband compatibility, see the [SEP/BB Compatibility Chart](https://docs.google.com/spreadsheets/d/1Mb1UNm6g3yvdQD67M413GYSaJ4uoNhLgpkc7YKi3LBs/)
- For iOS 13 and lower, check for compatibility with Limefix
    - As of updating this post, Limefix supports A9 devices and iOS 10, check for updates for other devices and versions
- Tethered downgrade/dualboot: [Semaphorin](https://github.com/LukeZGD/Semaphorin) supports these devices for downgrading to lower iOS versions
- Tethered downgrade/dualboot is also an option with [downr1n](https://github.com/edwin170/downr1n) or [dualra1n](https://github.com/dualra1n/dualra1n)
- See links above for other tethered 64-bit downgrade guides

###	**iOS 12 devices** (A7 and A8 devices except iPad Air 2 and mini 4)
- Devices that fall under here have the latest iOS version 12.5.7.
- You can downgrade to as low as 11.3 with futurerestore if you have SHSH blobs
    - You can downgrade with SHSH blobs using [Legacy iOS Kit](https://github.com/LukeZGD/Legacy-iOS-Kit)
- Tethered downgrade/dualboot: [Semaphorin](https://github.com/LukeZGD/Semaphorin) supports these devices for downgrading to lower iOS versions

###	**A7 devices** (iPhone 5S, iPad Air 1, iPad mini 2)
-	A7 devices (except iPad4,6 and all iPad mini 3 models) can downgrade to 10.3.3 without existing blobs, using [Legacy iOS Kit](https://github.com/LukeZGD/Legacy-iOS-Kit) or [LeetDown](https://github.com/rA9stuff/LeetDown)
    - Unfortunately checkm8 is very unreliable on Linux, so using macOS may be the better option with iPwnder32/ipwnder_lite used for better success rates
    - [iPwnder Lite for iOS](https://github.com/LukeZGD/Legacy-iOS-Kit/wiki/Pwning-Using-Another-iOS-Device) can also be used to put device in pwned DFU
-	You can also downgrade to 10.2-10.3.2 if you have SHSH blobs
    - You can go as low as 10.1.x but Touch ID will not work for 5S
    - You can downgrade with SHSH blobs using [Legacy iOS Kit](https://github.com/LukeZGD/Legacy-iOS-Kit)
- You can also downgrade/dualboot **tethered** with Semaphorin as mentioned above

###	**A5, A5X, A6, and A6X devices**
- Devices that fall under here have the latest iOS version 9.3.5, 9.3.6, 10.3.3, or 10.3.4.
- You can downgrade and jailbreak any of these devices (**except for iPhone 5C**) to iOS 8.4.1 using [Legacy iOS Kit](https://github.com/LukeZGD/Legacy-iOS-Kit) without existing blobs
    - For iPhone 4S and iPad 2 (except iPad2,4) devices, iOS 6.1.3 is also an option
- You can downgrade to any iOS version that you have SHSH blobs for using Legacy iOS Kit
- You can also save on-board SHSH blobs with Legacy iOS Kit's "Save Onboard Blobs" option
- If you have iOS 7 SHSH blobs, you can do untethered downgrades using Legacy iOS Kit (powdersn0w)
- You can also dualboot to any version using [Coolbooter](https://coolbooter.com)
- iPad 2 only: You can dualboot to iOS 4.3.x using [FourThree-iPad2](https://github.com/LukeZGD/FourThree-iPad2) or [4tify-iPad2](https://github.com/Zurac-Apps/4tify-iPad2)
    - 4tify-iPad2 is for macOS only
- You can downgrade **tethered** without blobs using [Legacy iOS Kit](https://github.com/LukeZGD/Legacy-iOS-Kit), [n1ghtshade](https://github.com/synackuk/n1ghtshade), or [Deca5](https://github.com/zzanehip/Deca5)
    - Both n1ghtshade and Deca5 are for macOS only

###	**iPhone 4**
- You can downgrade to iOS 6, 5, or 4 untethered using [Legacy iOS Kit](https://github.com/LukeZGD/Legacy-iOS-Kit) (powdersn0w)
    - Only iPhone3,1 (GSM) and iPhone3,3 (CDMA) are supported
- You can also use Legacy iOS Kit to downgrade with saved SHSH blobs (see above)
- You can also use Legacy iOS Kit to do **tethered** downgrades
    - Downgrading tethered by going to: Restore/Downgrade -> Other (Tethered)
    - Do a tethered boot by going to: Other Utilities -> Just Boot
    - Tethered downgrade IPSWs option is also available in Legacy iOS Kit, see below notes
- You can dualboot to any version except 4.x using [Coolbooter](https://coolbooter.com)
- You can dualboot to 4.x using [4tify](https://github.com/Zurac-Apps/4tify) (macOS only)
    - 4tify for Linux (untested): https://github.com/LukeZGD/4tify-linux

### **iPod touch 4th gen**
- There are no untethered downgrade options without blobs, unfortunately.
- iPod touch 4th gen can be tethered upgrade to iOS 7: https://albyvar.github.io/ipodtouchhax/
- You can do **tethered** downgrades without blobs using [Legacy iOS Kit](https://github.com/LukeZGD/Legacy-iOS-Kit)
    - Downgrading tethered by going to: Restore/Downgrade -> Other (Tethered)
    - Do a tethered boot by going to: Other Utilities -> Just Boot
    - There is also the option to use tethered downgrade IPSWs from these (Restore/Downgrade -> Other (Custom IPSW)):
    - https://www.reddit.com/r/LegacyJailbreak/comments/ji17zv/discussion_would_you_guys_be_interested_in/
    - https://github.com/lychitree/pr3lude
    - https://archives.legacyjailbreak.com

### **iPhone 3GS, iPad 1, iPod touch 2nd and 3rd gen**
- You can also use [Legacy iOS Kit](https://github.com/LukeZGD/Legacy-iOS-Kit) to do untethered downgrades to other iOS versions without blobs (or with blobs also)
- iOS 4.1 is signed for the 3GS, touch 2nd gen and 3rd gen, use iTunes or Legacy iOS Kit to downgrade
    - Error 1015 will show up at the end of restore for iTunes. You can exit recovery mode using [RecoveryModeTool](https://invoxiplaygames.uk/projects/recoverymodetool/)
- Tethered downgrade option is also available in Legacy iOS Kit, see above notes
    - For the iPod touch 3rd gen, see tethered custom IPSWs in pr3lude
- For iPod touch 2nd gen (old bootrom model), there is no firmware signing for versions 3.0 and lower can be restored freely. 3.x IPSWs are available here: https://invoxiplaygames.uk/ipsw/ and https://archives.legacyjailbreak.com

### **iPhone 2G, 3G, and iPod touch 1st gen**
- AFAIK, there is no firmware signing and they can be restored to any iOS version using an appropriate older iTunes version after placing the device in pwned DFU mode using redsn0w.
- [Legacy iOS Kit](https://github.com/LukeZGD/Legacy-iOS-Kit) also supports these devices for restoring custom IPSWs as well as restoring to any other iOS version
    - Jailbreak support is for iOS 4.2.1, 4.1 and 3.1.3 for these devices
    - Lowest working version is 2.0, 1.x does not work
    - This makes use of changes from tihmstar's idevicerestore fork (iOS 1 and 2 support): https://github.com/tihmstar/idevicerestore
- iPhone 3G devices can be easily downgraded to iOS 4.1 and 3.1.3 using Legacy iOS Kit, with the option to jailbreak and hacktivate
- For iPhone 2G and touch 1, here are 3.1.3 custom IPSWs that may be useful: https://github.com/LukeZGD/Legacy-iOS-Kit-Keys/releases/tag/jailbreak
- IPSWs for older iPod touch are not available in ipsw.me. They are available here: https://invoxiplaygames.uk/ipsw/ and https://archives.legacyjailbreak.com
